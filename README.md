<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# Element Web Image

This repository creates the Element Web image that includes custom modules for openDesk deployments.

## Content:
- [Element Web](https://github.com/element-hq/element-web)
- [Element Web Guest Module](https://github.com/nordeck/element-web-modules/tree/main/packages/element-web-guest-module)
- [Element Web openDesk Module](https://github.com/nordeck/element-web-modules/tree/main/packages/element-web-opendesk-module)
- [Element Web Widget Lifecycle Module](https://github.com/nordeck/element-web-modules/tree/main/packages/element-web-widget-lifecycle-module)
- [Element Web Widget Toggles Module](https://github.com/nordeck/element-web-modules/tree/main/packages/element-web-widget-toggles-module)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
