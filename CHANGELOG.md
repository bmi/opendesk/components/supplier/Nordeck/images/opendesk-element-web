## [1.11.7](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.11.6...v1.11.7) (2025-01-17)


### Bug Fixes

* **element:** Fix build for and update Element Web to 1.11.90 ([f77e42a](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/f77e42a03ec7eee3b197cd0acae579caa51982d5))

## [1.11.6](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.11.5...v1.11.6) (2024-12-30)


### Bug Fixes

* Update and pin base images ([d07f5bd](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/d07f5bd061ef7bbcbbfcc724226cde641a71b397))

## [1.11.5](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.11.4...v1.11.5) (2024-12-30)


### Bug Fixes

* Update Element (Web) to v1.11.89 ([1c6f611](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/1c6f611e9aaa4ff467de98d3fdedf8fdc572d077))

## [1.11.4](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.11.3...v1.11.4) (2024-12-12)


### Bug Fixes

* **element:** Change base image from bookworm to alpine to fix CVE-2023-6879 ([a110e09](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/a110e09b2a3280041d8b0de4d4129c7bb40e22bf))

## [1.11.3](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.11.2...v1.11.3) (2024-12-05)


### Bug Fixes

* Bump Element Web to 1.11.87 ([df6a8a3](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/df6a8a33347b18138be3eb7eee8fb1aedcb4d12f))
* Bump Nordeck modules as required for the Element update. ([ad799f7](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/ad799f7a5e3fc03ffd4029706bc3279d016531dd))
* **element:** Make pipeline happy ([2f28744](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/2f2874444077670d037636f2a16566b0f9e354ef))

## [1.11.2](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.11.1...v1.11.2) (2024-10-01)


### Bug Fixes

* **element:** Update Element Web to 1.11.76 ([e8ea686](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/e8ea686c0fc1bf7f8b31f8bae3cd732e4f1b1b51))

## [1.11.1](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.11.0...v1.11.1) (2024-05-27)


### Bug Fixes

* Bump to Element v1.11.67 ([3fd98df](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/3fd98df4526bf68943a32abf34524cf1a34c183c))

# [1.11.0](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.10.0...v1.11.0) (2024-03-14)


### Bug Fixes

* **element:** Use short commit hashes and use them in version string ([759e908](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/759e90800d03a9385e7334eb4a02d7888bef2f89))


### Features

* **element:** Update to Element Web v1.11.59 with widget sync fix ([c0cb0ef](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/c0cb0ef8280ba756f0bf38b348969db9521d875a))

# [1.10.0](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.9.1...v1.10.0) (2024-02-28)


### Features

* **element:** Update to EW 1.11.59 with fixes for E2EE Guest Issue + Widget Toggles + Sticky Rooms ([524616d](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/524616d1a83523237443013f0b1d8477a145a71e))

## [1.9.1](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.9.0...v1.9.1) (2024-01-17)


### Bug Fixes

* **element:** Update to latest public release v1.11.54-rc.0 ([a60c8cd](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/a60c8cd9580f09123aa604765578d628ce401567))

# [1.9.0](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.8.2...v1.9.0) (2024-01-11)


### Features

* Build from Nordeck repo, nic/feat/ms6-e2ee branch, off upstream develop branch ([e7f2eea](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/e7f2eea2e5eda5827b962fbb0aef33c6b166a5f6))

## [1.8.2](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.8.1...v1.8.2) (2024-01-02)


### Bug Fixes

* **docker:** Add linebreak ([f37d7d8](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/f37d7d88e2e4a5cdf166dc5dfb28d9a443a51457))

## [1.8.1](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/compare/v1.8.0...v1.8.1) (2024-01-02)


### Bug Fixes

* **ci:** Build on Open CoDE ([434e215](https://gitlab.opencode.de/bmi/opendesk/components/supplier/Nordeck/images/opendesk-element-web/commit/434e215981ab4eb80f3ae05408c5896ca2e3aaec))

# [1.8.0](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/compare/v1.7.0...v1.8.0) (2023-12-21)


### Bug Fixes

* **ci:** Add container-build section ([cad9ead](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/cad9ead8e82da4c5c83e80abcb601bbb638169cd))
* **ci:** Update CI for updated gitlab-config ([d21b5bf](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/d21b5bf152d8fbf3caf293a16e7d77f885058595))


### Features

* Update to Element Web v1.11.52 ([25bfd7a](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/25bfd7a6d6787abbad7f19989bfca28513db92af))
* Updating GitHub URLs for Element Web ([72f2485](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/72f2485ea918e8cfd2c312f841204255954764dd))

# [1.7.0](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/compare/v1.6.0...v1.7.0) (2023-12-06)


### Features

* Remove unnecessary whitespace in Dockerfile ([44a5c8e](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/44a5c8e1ffd46817dde7b1f101053e59a5a567b9))
* Update element-web-opendesk-module to v0.3.0 ([0947de5](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/0947de542e506479c8b365ecde1ad03894b52574))
* Update to Element Web v1.11.51 ([5ae64f6](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/5ae64f69d1fa46e210bc3625c840e8e4ba16d2f7))
* Update to Element Web v1.11.51-rc.0 and revert back to build from a public release ([a7ae89c](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/a7ae89ca91f5450f04e39135214b59ec608fa9b0))

# [1.6.0](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/compare/v1.5.0...v1.6.0) (2023-11-09)


### Features

* Build from Nordeck repo and update openDesk module with widgets toggle support ([e41fd83](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/e41fd837efe83ffed27f307108c4b61e168a528c))

# [1.5.0](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/compare/v1.4.0...v1.5.0) (2023-11-01)


### Features

* Update to Element Web v1.11.48-rc.1 ([c278a27](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/c278a27b782fe702db7b352d37e2dba190d26320))

# [1.4.0](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/compare/v1.3.0...v1.4.0) (2023-10-26)


### Features

* Update to Element Web v1.11.47 ([cee9fd4](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/cee9fd47c0a3e4fc6992592fe1101485ec309f0e))

# [1.3.0](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/compare/v1.2.0...v1.3.0) (2023-10-20)


### Features

* Install the Element Web openDesk Module ([f21cd25](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/f21cd253008d264b89e0632b03a3ecb6cf058591))

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/compare/v1.1.0...v1.2.0) (2023-10-13)


### Bug Fixes

* Switch to nginx:1-bookwork ([b475938](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/b475938341d9a27247c16bcd9edc086c6abfcf44))


### Features

* Upgrade Element to v1.11.46 ([f571093](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/f5710936ff0e460f93c8fd5a75c82a29b93147bb))

# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/compare/v1.0.0...v1.1.0) (2023-09-25)


### Features

* Enable the guest module ([34f7af6](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/34f7af623bdac8c75047cf1a3852b99729fa800f))
* Switch to the Open Source version of the element-widget-lifecycle-module ([ecd0488](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/ecd0488c95d2bda3119c04151192ac93de8814b1))

# 1.0.0 (2023-09-20)


### Bug Fixes

* Allow data urls for the connect-src in the CSP ([55d215e](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/55d215e8387f86c284f36ba654fbcb1e13864f2c))
* Support a custom css file to load additional styles ([386f950](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/386f950605f52de0e130b672dc8189b1335eb43d))
* Use correct index.html path ([1ec3934](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/1ec39348bf3fc1c0c0904c4c466c64dc3f4ef4ab))


### Features

* Add initial version ([4ceb59f](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/4ceb59f17f7de75fcde1e58c312ea8c74de2bdfd))
* Move files used in dockerfile to src folder and use git lfs for tgz files ([4d9f767](https://gitlab.souvap-univention.de/souvap/tooling/images/element-web/commit/4d9f767a3a093f6a29ee0795d19c91ce8387790f))
