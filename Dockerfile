# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

FROM --platform=$BUILDPLATFORM node:23.5.0-bullseye@sha256:60b39aa2d402121fb400b500e96630976524c5d7c31df6e7b34a621b9664b7ba as builder
#LABEL maintainer="tbd <info@tbd.de>"

ARG ELEMENT_VERSION="v1.11.90"

WORKDIR /src

RUN git clone --depth 1 --branch $ELEMENT_VERSION https://github.com/element-hq/element-web.git /src && yarn --network-timeout=200000 install

COPY /src/build_config.yaml /src
COPY /src/customisations.json /src
# optionally copy webpack with minification disabled
# COPY /src/webpack.config.js /src
COPY /src/modules /src/modules

RUN bash /src/scripts/docker-package.sh

# App
FROM nginx:1.27.3-alpine-slim@sha256:e9d4fe3e963d75580048fa9a860c514312c328f536595022e597d1c4729f073a

COPY --from=builder /src/webapp /app

# Override default nginx config
COPY --from=builder /src/docker/nginx-templates/* /etc/nginx/templates/

RUN rm -rf /usr/share/nginx/html \
 && ln -s /app /usr/share/nginx/html

ENV ELEMENT_WEB_PORT=80
